<?php
/**
 * @package sprout
 * @subpackage classfile
 */

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

/**
 * class Sprout
 */
class Sprout extends \FractalFarming\Sprout\Sprout
{
}
