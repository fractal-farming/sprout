# Changelog for Sprout

## Sprout 0.3.2

- Stop using extension package to autoload base class
- Set minimum PHP version to 8.1
- Update Composer dependencies

## Sprout 0.3.1

- Detect Obsidian callouts (styled blockquotes)
- Detect citation source in blockquote footers
- Tweaks and fixes to article import

## Sprout 0.3.0

- Fix image references containing a title
- Allow usage of identifier prefix in links
- Implement media source into attachments path (optional)
- Ensure full attachments path is always set
- Add system setting to specify attachment path
- Switch to GPM v1 (MODX 2)

## Sprout 0.2.0

- Process links and Obsidian references
- Add snippet for importing articles
- Add plugin for generating static HTML
- Add snippet for processing Markdown
- Replace Parsedown with League/CommonMark
- Fix autoloading

Internal release.

## Sprout 0.1.0

Initial commit.