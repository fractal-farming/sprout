<?php

$_lang['area_sprout'] = 'Sprout';
$_lang['area_sprout.static'] = 'Static resources';

$_lang['setting_sprout.static.context'] = 'Context';
$_lang['setting_sprout.static.context_desc'] = 'Default context for static resources.';
$_lang['setting_sprout.static.content_type'] = 'Content type';
$_lang['setting_sprout.static.content_type_desc'] = 'Default content type for static resources. If you use Romanesco, this needs to be set to the Markdown content type. Otherwise, you can leave it at the default (HTML).';
$_lang['setting_sprout.static.createdby'] = 'Created by';
$_lang['setting_sprout.static.createdby_desc'] = 'Default user when generating new resources.';
$_lang['setting_sprout.static.source'] = 'Media source';
$_lang['setting_sprout.static.source_desc'] = 'This source points to the root folder of the static Markdown files. If that root folder is a symlink, make sure to disable basePathRelative.';
$_lang['setting_sprout.static.attachments_path'] = 'Attachments path';
$_lang['setting_sprout.static.attachments_path_desc'] = 'Path to folder containing the assets referenced inside the Markdown files. Relative to media source root.';

$_lang['setting_sprout.link_prefix'] = 'Link prefix';
$_lang['setting_sprout.link_prefix_desc'] = 'If you\'re using an identifier to tell your links apart within your note taking system, but want to remove it from your live URLs: enter it here.';
