<?php

$_lang['sprout.menu.title'] = 'Sprout';
$_lang['sprout.menu.description'] = 'Static markdown site generator';

$_lang['sprout.overview.button_text'] = 'Read more';

$_lang['sprout.footer.back_to_top'] = 'Back to top';
$_lang['sprout.footer.goodbye_world'] = 'Goodbye World';
