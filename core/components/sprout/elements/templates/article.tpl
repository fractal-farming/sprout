<!DOCTYPE html>
<html lang="[[++cultureKey]]" data-theme="[[++sprout.theme]]">

<head>
    [[$sproutHead]]
</head>

<body id="[[*alias]]" class="article">

[[$sproutMenu]]

<main class="container">
    [[*content:sproutProcessMarkdown]]
</main>

[[$sproutFooter]]

</body>
</html>