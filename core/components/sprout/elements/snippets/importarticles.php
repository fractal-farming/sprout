<?php
/**
 * sproutImportArticles
 *
 * Scan static markdown folder and create a resource for each new article.
 *
 * For an article to be imported, it needs to contain a configuration object for
 * MODX in YAML format (inside the front matter section). For example:
 *
 * modx:
 *   id: 0
 *   pagetitle: 'Markdown content'
 *   longtitle: ''
 *   description: ''
 *   introtext: ''
 *   parent: 2
 *   template: 2
 *   published: 1
 *
 * Make sure the ID parameter is either 0, empty or absent. After generating the
 * resource, the ID will be updated in or added to the front matter.
 *
 * If a resource already exists, it will update the resource with any changes
 * made in the front matter data. That means this data takes precedence over
 * changes made in MODX. So if you add a description in MODX while it's still
 * empty in the front matter, your changes will be lost on the next import.
 * If you want to avoid this behaviour, simply remove the line from the config.
 *
 * This is a utility snippet. Simply call it on any page to execute.
 *
 * This snippet depends on the DirWalker extra from Bob Ray.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

use FractalFarming\Sprout\Sprout;
use League\CommonMark\Extension\FrontMatter\Data\SymfonyYamlFrontMatterParser;
use League\CommonMark\Extension\FrontMatter\FrontMatterParser;
use Symfony\Component\Yaml\Yaml;

require_once MODX_CORE_PATH . 'components/dirwalker/model/dirwalker/dirwalker.class.php';

if (!class_exists(DirWalker::class)) {
    $modx->log(modX::LOG_LEVEL_ERROR, 'DirWalker class not found.');
    return false;
}

$contextKey = $modx->getOption('sprout.static.context', $scriptProperties, 'notes');
$contentType = $modx->getOption('sprout.static.content_type', $scriptProperties, 11);
$createdBy = $modx->getOption('sprout.static.createdby', $scriptProperties, 1);
$mimeType = 'text/html';
$sourceID = $modx->getOption('sprout.static.source', $scriptProperties, '');
$sourcePath = '';

// Get mime type (MODX 2)
if ($contentType) {
    $type = $modx->getObject('modContentType', $contentType);
    $mimeType = $type->get('mime_type');
}

// Get media source path
if ($sourceID) {
    $source = $modx->getObject('modMediaSource', $sourceID);
    $source->initialize();
    $sourcePath = $source->getBasePath();
}

// Walk through source folders in search of Markdown files
$searchStart = MODX_BASE_PATH . $sourcePath;
$dw = new DirWalker();
$dw->setIncludes('.md');
$dw->setExcludes('.gitignore');
$dw->setExcludeDirs('.git,assets');
$dw->dirWalk($searchStart, true);

$fileArray = $dw->getFiles();

foreach($fileArray as $path => $filename) {
    $content = file_get_contents($path);
    $relativePath = str_replace(MODX_BASE_PATH, '', $path);
    $config = [];

    // Get front matter
    $frontMatterParser = new FrontMatterParser(new SymfonyYamlFrontMatterParser());
    $result = $frontMatterParser->parse($content);
    $frontMatter = $result->getFrontMatter();

    // Skip file if it doesn't contain a MODX config
    if (!$frontMatter['modx']) {
        continue;
    }

    // Update existing resource or create new one
    if ($frontMatter['modx']['id']) {
        $resource = $modx->getObject('modResource', $frontMatter['modx']['id']);

        if (is_object($resource)) {
            $resource->fromArray($frontMatter['modx']);
            $resource->save();
            echo 'Resource updated: ' . $resource->get('id') . '<br>';
        } else {
            echo 'Resource not found: ' . $frontMatter['modx']['id'] . '<br>';
            continue;
        }
    }
    else {
        $resource = $modx->newObject('modResource');
        $resource->set('content_type', $contentType);
        $resource->set('contentType', $mimeType);
        $resource->set('class_key', 'modStaticResource');
        $resource->set('context_key', $contextKey);
        $resource->set('content', $relativePath);
        $resource->set('richtext', 0);
        $resource->set('createdby', $createdBy);
        $resource->set('show_in_tree', 0);
        $resource->fromArray($frontMatter['modx']);
        $resource->save();

        // Write ID back to the markdown file
        if ($resource->get('id')) {
            $frontMatter['modx']['id'] = $resource->get('id');

            // Split original content to isolate front matter
            $split = preg_split('/[\n]*[-]{3}[\n]/', $content, 3);

            // Overwrite front matter with updated ID
            file_put_contents($path, str_replace($split[1], rtrim(yaml::dump($frontMatter, 2, 2)), $content));

            echo 'Resource created: ' . $resource->get('id') . '<br>';
        } else {
            echo 'Massive failure!';
        }
    }

    // Set publish date
    if ($frontMatter['modx']['published'] && !$resource->get('publishedon')) {
        $resource->set('publishedon', time());
        $resource->set('publishedby', $frontMatter['modx']['publishedby'] ?? $createdBy);
        $resource->save();
    }
}

return;