<?php
/**
 * sproutProcessMarkdown
 *
 * Convert Markdown to HTML using the PHP League commonmark parser:
 * https://commonmark.thephpleague.com/
 *
 * Note that the front matter extension is loaded, but not used. This hides any
 * front matter present in the markdown file. In the future, it could be
 * rendered through a placeholder. For details, see:
 * https://commonmark.thephpleague.com/2.2/extensions/front-matter/
 */

use FractalFarming\Sprout\Sprout;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use League\CommonMark\Extension\FrontMatter\FrontMatterExtension;
use League\CommonMark\Extension\FrontMatter\Output\RenderedContentWithFrontMatter;
use League\CommonMark\MarkdownConverter;
use Wa72\HtmlPageDom\HtmlPageCrawler;

/**
 * @var modX $modx
 * @var array $scriptProperties
 * @var string $input
 * @var string $options
 */

$corePath = $modx->getOption('sprout.core_path', null, $modx->getOption('core_path') . 'components/sprout/');
$sprout = $modx->getService('sprout','Sprout',$corePath . 'model/sprout/', ['core_path' => $corePath]);
if (!($sprout instanceof Sprout)) return;

$sprout = new Sprout($modx);
$input = $modx->getOption('markdown', $scriptProperties, $input);
$sourceID = $modx->getOption('sprout.static.source', $scriptProperties, '');
$attachmentsPath = $modx->getOption('sprout.static.attachments_path', $scriptProperties, '');
$linkPrefix = $modx->getOption('sprout.link_prefix', $scriptProperties, '');

// Implement media source into attachments path (optional)
$sourcePath = '';
if ($sourceID) {
    $source = $modx->getObject('modMediaSource', $sourceID);
    $source->initialize();
    $sourcePath = $source->getBasePath();
}
$attachmentsPathFull = $sourcePath . $attachmentsPath;

// Get file extension
$query = $modx->newQuery('modContentType');
$query->where(['id' => $modx->resource->get('content_type')]);
$query->select('file_extensions');
$ext = $modx->getValue($query->prepare());

// Define your CommonMark configuration, if needed
$config = [
    'external_link' => [
        'internal_hosts' => $modx->getOption('site_url', $scriptProperties),
        'open_in_new_window' => true,
        'html_class' => 'external',
        'nofollow' => '',
        'noopener' => 'external',
        'noreferrer' => 'external',
    ],
];

// Configure the Environment with all the CommonMark and GFM parsers/renderers
$environment = new Environment($config);
$environment->addExtension(new CommonMarkCoreExtension());
$environment->addExtension(new GithubFlavoredMarkdownExtension());
$environment->addExtension(new ExternalLinkExtension());
$environment->addExtension(new FrontMatterExtension());

// Parse content
$converter = new MarkdownConverter($environment);
try {
    $html = $converter->convert($input);
} catch (\League\CommonMark\Exception\CommonMarkException $e) {

}

// Parse front matter (for future reference)
//if ($html instanceof RenderedContentWithFrontMatter) {
//    $modx->toPlaceholder('front_matter', $html->getFrontMatter());
//}

// Escape MODX tags
$html = $sprout->escapeTags($html);

// Post-processing with HtmlPageDom
$dom = new HtmlPageCrawler($html);

// Transform Obsidian links to HTML
$dom->filter('h1, h2, h3, h4, h5, h6, p, li, blockquote')
    ->each(function (HtmlPageCrawler $element) use ($ext, $attachmentsPathFull)
    {
        $content = $element->getInnerHtml();

        $matchTitledEmbed = '/\!\[\[([^\]]+)\|([^\]]+)\]\]/'; // ![[some-image.jpg|Alt text]]
        $matchEmbed = '/\!\[\[([^\]]+)\]\]/'; // ![[some-image.jpg]]
        $matchPipedRef = '/\[\[([^\]]+)\|([^\]]+)\]\]/'; // [[some-article|See here]]
        $matchRef = '/\[\[([^(\]|&|+)]+)\]\]/'; // [[Regular reference]]

        // Look for matches and bundle them with PREG_SET_ORDER
        preg_match_all($matchTitledEmbed, $content,$titledEmbeds, PREG_SET_ORDER);
        preg_match_all($matchEmbed, $content,$embeds, PREG_SET_ORDER);
        preg_match_all($matchPipedRef, $content,$pipedRefs, PREG_SET_ORDER);
        preg_match_all($matchRef, $content,$refs, PREG_SET_ORDER);

        $dirty = 0;
        foreach ($titledEmbeds as $embed)
        {
            if (!$embed) continue;
            $content = str_replace($embed[0], '<img src="'.$attachmentsPathFull.$embed[1].'" alt="'.$embed[2].'" />', $content);
            $dirty = 1;
        }
        foreach ($embeds as $embed)
        {
            if (!$embed) continue;
            $content = str_replace($embed[0], '<img src="'.$attachmentsPathFull.$embed[1].'" />', $content);
            $dirty = 1;
        }
        foreach ($pipedRefs as $ref)
        {
            if (!$ref) continue;
            $content = str_replace($ref[0], '<a href="'.$ref[1].$ext.'">'.$ref[2].'</a>', $content);
            $dirty = 1;
        }
        foreach ($refs as $ref)
        {
            if (!$ref) continue;
            $content = str_replace($ref[0], '<a href="'.$ref[1].$ext.'">'.$ref[1].'</a>', $content);
            $dirty = 1;
        }

        // Only replace HTML in altered elements
        if ($dirty) {
            $element->makeEmpty();
            $element->setInnerHtml($content);
        }
    })
;

// Modify links
$dom->filter('a')
    ->each(function (HtmlPageCrawler $link) use ($modx, $ext, $linkPrefix)
    {
        $href = $link->getAttribute('href');

        // Replace .md extension
        $href = str_replace('.md', $ext, $href);

        // Remove identifier
        if ($linkPrefix && str_starts_with($href, $linkPrefix)) {
            $href = str_replace($linkPrefix, '', $href);
        }

        // Update link
        $link->setAttribute('href', $href);

        // Internal link
        if ($link->hasClass('external') === false)
        {
            // Prepend anchor link with URI
            if (str_starts_with($href, '#')) {
                $link
                    ->addClass('contrast')
                    ->setAttribute('href', $modx->resource->get('uri') . $href)
                ;
                return;
            }

            // Mute non-existing link
            $query = $modx->newQuery('modResource');
            $query->where(['uri' => $href]);
            $query->select('id');
            if (!$modx->getValue($query->prepare())) {
                $link->addClass('secondary');
            }
        }
    })
;

// Modify images
$dom->filter('img')
    ->each(function (HtmlPageCrawler $image) use ($attachmentsPathFull)
    {
        $src = $image->getAttribute('src');

        // Add full attachment path to regularly formatted images
        if (!str_contains($src, $attachmentsPathFull)) {
            $src = $attachmentsPathFull . basename($src);
            $image->setAttribute('src', $src);
        }

        // Add class to parent element
        $image->closest('p')->addClass('image');
    })
;

// Detect footer inside quotes
$dom->filter('blockquote p:last-child')
    ->each(function (HtmlPageCrawler $quote)
    {
        $content = $quote->getInnerHtml();

        // Yes, that's to different ― symbols looking exactly the same
        if (str_starts_with($content, '—') || str_starts_with($content, '―') || str_starts_with($content, '--'))
        {
            // Assume second part of string is the source of the citation (cite)
            if (str_contains($content, ',')) {
                $split = explode(',', $content, 2);
                $content = $split[0] . ', <cite>' . $split[1] . '</cite>';
            }

            // Convert double dash to the single big one that's always impossible to find
            $content = str_replace('--', '—', $content);

            // We have our footer
            $quote->replaceWith("<footer>$content</footer>");
        }
    })
;

// Detect Obsidian callouts
$dom->filter('blockquote p:first-child')
    ->each(function (HtmlPageCrawler $quote)
    {
        $content = $quote->getInnerHtml();

        if (str_starts_with(trim($content), '[!'))
        {
            preg_match('/\[!(.+)]/', $content, $matches);

            $class = trim(strtolower($matches[1])) ?? '';
            $header = trim(str_replace($matches[0], '', $content)) ?? '';
            $content = ''; // reset to clear callout tag

            // Any text after the callout tag is considered the header
            if ($header) {
                $content = "<p><strong>$header</strong></p>";
            }

            // Set callout type as class on parent
            if ($class) {
                $quote->closest('blockquote')->addClass("callout $class");
            }

            // Replace callout with header, or remove it if empty
            if ($content) {
                $quote->replaceWith($content);
            } else {
                $quote->remove();
            }
        }
    })
;

// Update HTML
$html = $sprout->escapeTags($dom->saveHTML());

return $html;